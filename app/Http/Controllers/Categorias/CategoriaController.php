<?php

namespace App\Http\Controllers\Categorias;

use Illuminate\Http\Request;
use App\Categoria;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriaController extends Controller
{
    function index() 
    {
        $Categorias = Categoria::all();
        return response()->json($Categorias);
    }
}

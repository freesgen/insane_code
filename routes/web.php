<?php

Route::get('/', function () {
    return view('/layouts/app');
});
// CRUD Categorias
Route::get('/categorias', 'Categorias\CategoriaController@index');
Route::get('/categorias/{id}', 'Categorias\CategoriaController@index');
Route::post('/categorias/create/{categoria}', 'Categorias\CategoriaController@add');
Route::delete('/categorias/{id}', 'Categorias\CategoriaController@delete');
Route::patch('/categorias/{categoria}', 'Categorias\CategoriaController@update');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/path', function () {
    return DB::query('show tables');   
});
